set(source_files
  ${CMAKE_CURRENT_SOURCE_DIR}/src/ecpp/Drivers/ESP32/PowerManager.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/ecpp/Drivers/ESP32/Spi.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/ecpp/Drivers/ESP32/Timer.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/src/ecpp/Drivers/ESP32/Twi.cpp
  
  ${CMAKE_CURRENT_SOURCE_DIR}/src/Platform.cpp
)

set(include_dirs
  ${CMAKE_CURRENT_SOURCE_DIR}/src
)

idf_component_register(SRCS "${source_files}"
                    INCLUDE_DIRS "${include_dirs}"
                    REQUIRES "ecpp-core ecpp-job sys")

#target_compile_definitions(${COMPONENT_TARGET} PUBLIC "-DESP32")