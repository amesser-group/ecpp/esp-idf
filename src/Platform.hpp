/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_PLATFORM_HPP_
#define ECPP_PLATFORM_HPP_

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"

#include "ecpp/Execution/Job.hpp"
#include "ecpp/Execution/JobTimer.hpp"
#include "ecpp/Execution/LockGuard.hpp"

namespace sys
{
  using ::ecpp::SystemTick;
  using ::ecpp::GetSystemTick;

  using Tick = SystemTick;

  static inline SystemTick GetTicks() { return GetSystemTick(); }

  using   ::ecpp::Execution::Job;
  using   Timer = ::ecpp::Execution::TimerQueue::Timer;

  class Mutex
  {
  public:
    Mutex();

    void Lock()   { xSemaphoreTake(lock_, portMAX_DELAY); }
    void Unlock() { xSemaphoreGive(lock_); }

  protected:
    SemaphoreHandle_t const lock_;
  };

  class MutexLockable
  {
  public:
    using Lockable  = MutexLockable;
    using LockGuard = ::ecpp::Execution::LockGuard<MutexLockable>;

  protected:
    class Locker
    {
    public:
      constexpr Locker(MutexLockable& owner) : mutex_(owner.mutex_) {}
      void      Lock()   { mutex_.Lock(); }
      void      Unlock() { mutex_.Unlock(); }
    private:
      Mutex&        mutex_;
    };

    Mutex           mutex_;

    friend class ::ecpp::Execution::LockGuard<MutexLockable>;
  };  

  class JobQueue : protected ::ecpp::Execution::JobQueue, public MutexLockable
  {
  public:
    JobQueue();

    void Enqueue(Job & job, Job::Handler handler);
    Job* GetNext();
  };

  class TimerQueue : public ::ecpp::Execution::TimerQueue, public MutexLockable
  {
  public:
    Job*                        CheckNextTimeout(SystemTick &next_timeout) { return ::ecpp::Execution::TimerQueue::CheckNextTimeout(*this, next_timeout); }
  };

  class Platform
  {
  public:
    constexpr Platform() {}
  };

  template<typename OBJ>
  static inline void Enqueue(const OBJ& obj, ::ecpp::Execution::Job& job, ::ecpp::Execution::Job::Handler handler);
}
#endif