/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_DRIVERS_ESP32_TIMER_HPP_
#define ECPP_DRIVERS_ESP32_TIMER_HPP_

#include "driver/timer.h"
#include <cstdint>

namespace ecpp::Drivers::ESP32
{
  class TimerImpl
  {
  protected:
    void SetMaxCount(timer_group_t timer_group, timer_idx_t timer_idx, ::std::uint64_t max_count);
  };

  template<timer_group_t timer_group, timer_idx_t timer_idx>
  class Timer : public TimerImpl
  {
  public:
    static constexpr timer_group_t kTimerGroup = timer_group;
    static constexpr timer_idx_t   kTimerIdx   = timer_idx;

    constexpr Timer() {};

    void Init(const timer_config_t & init)
    {
      timer_init(kTimerGroup, kTimerIdx, &init);
    }

    void SetMode(uint16_t divider, bool decrement)
    {
      timer_set_divider(kTimerGroup, kTimerIdx, divider);
      timer_set_counter_mode(kTimerGroup, kTimerIdx, decrement ? TIMER_COUNT_DOWN : TIMER_COUNT_UP);
    }

    void SetMaxCount(::std::uint64_t max_count)
    {
      TimerImpl::SetMaxCount(kTimerGroup, kTimerIdx, max_count);
    }

    void Start()
    {
      timer_start(kTimerGroup, kTimerIdx);
    }
  };

}
#endif
