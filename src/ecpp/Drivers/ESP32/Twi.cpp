/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/Drivers/ESP32/Twi.hpp"
#include "System.hpp"

using namespace ecpp::Drivers::ESP32;

TwiRequestBlock::Status
TwiRequestBlock::Execute(TwiMaster& master)
{
  TwiMaster::LockGuard lock(master);

  i2c_cmd_handle_t cmd = i2c_cmd_link_create();
  esp_err_t ret;

  if(write_len_ > 0)
  {
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (addr_ << 1) | I2C_MASTER_WRITE, true);
    i2c_master_write(cmd, reinterpret_cast<uint8_t*>(dma_buffer_), write_len_, true);
  }

  if(read_len_ > 0)
  {
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (addr_ << 1) | I2C_MASTER_READ, true);
    i2c_master_read(cmd, reinterpret_cast<uint8_t*>(dma_buffer_), read_len_, I2C_MASTER_LAST_NACK);
  }

  i2c_master_stop(cmd);

  ret = i2c_master_cmd_begin(master.i2c_port_, cmd, 1000 / portTICK_RATE_MS);
  i2c_cmd_link_delete(cmd);

  return Status(ret);
}

TwiRequestBlock::Status
TwiRequestBlock::Execute(TwiMaster& master, uint_least16_t write_len, uint_least16_t read_len)
{
  write_len_ = write_len;
  read_len_  = read_len;
  return Execute(master);
}

void
TwiMaster::Initialize(i2c_port_t port, const i2c_config_t &config)
{
  if(port >= I2C_NUM_MAX)
    return;

  if(i2c_driver_install(port, I2C_MODE_MASTER, 0, 0, 0) != ESP_OK)
    return;

  if(i2c_param_config(port, &config) != ESP_OK)
  {
    i2c_driver_delete(port);
    return;
  }

  i2c_port_ = port;
}

void
TwiMaster::Execute(RequestQueue::QueueItem &req)
{
  req.state_ = req.State::Pending;
  req.Execute(*this);
}

ECPP_JOB_DEF(TwiMaster, ProcessQueue)
{
  auto item = pending_requests_.PopFront(pending_requests_);

  if(item == nullptr)
    return;

  if(i2c_port_ < I2C_NUM_MAX)
    Execute(*item);
  else
    item->state_ = item->State::Error;

  signals::Complete(*item);

  ProcessQueue.Enqueue();
}

TwiRequestBlock::Status
TwiMaster::QueueAbleRequestBlock::Execute(TwiMaster& master)
{
  return Execute(master, write_len_, read_len_);
}


TwiRequestBlock::Status
TwiMaster::QueueAbleRequestBlock::Execute(TwiMaster& master, uint_least16_t write_len, uint_least16_t read_len)
{
  auto status  = TwiRequestBlock::Execute(master, write_len, read_len);

  if(status == Status::OK)
    state_ = State::Success;
  else
    state_ = State::Error;

  return status;
}


void
TwiMaster::RequestQueueItem::Submit(TwiMaster& drv, uint_fast16_t write_len, uint_fast16_t read_len)
{
  state_ = State::Queued;
  write_len_ = write_len;
  read_len_  = read_len;

  drv.pending_requests_.PushBack(drv.pending_requests_, *this);
  drv.ProcessQueue.Enqueue();
}
