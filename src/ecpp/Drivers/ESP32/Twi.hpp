/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_MCU_ESP32_TWI_HPP_
#define ECPP_MCU_ESP32_TWI_HPP_

#include <cstdint>

#include "ecpp/Execution/Job.hpp"
#include "ecpp/BinaryStruct.hpp"
#include "ecpp/Containers/Queue.hpp"

#include "Platform.hpp"

#include "driver/i2c.h"

namespace ecpp::Drivers::ESP32
{
  class TwiMaster;

  class TwiRequestBlock
  {
  public:
    enum Status : esp_err_t
    {
      OK   = ESP_OK,
      FAIL = ESP_FAIL,
    };

    void SetUp(uint8_t addr, void* dma_buffer)
    {
      addr_       = addr;
      dma_buffer_ = dma_buffer;
      write_len_  = 0;
      read_len_   = 0;
    }

    constexpr uint_least16_t GetReadLen()  const        { return read_len_;  }
    constexpr uint_least16_t GetWriteLen() const        { return write_len_; }

    constexpr bool IsSame(const TwiRequestBlock& other) { return this == &other; }

  protected:
    uint_least8_t      addr_;
    uint_least16_t     write_len_;
    uint_least16_t     read_len_;
    void              *dma_buffer_;

    Status       Execute(TwiMaster& master);
    Status       Execute(TwiMaster& master, uint_least16_t write_len, uint_least16_t read_len);

    friend class TwiMaster;
  };


  class TwiMaster : public ::sys::MutexLockable
  {
  protected:
    class QueueAbleRequestBlock;

  public:
    void Initialize(i2c_port_t port, const i2c_config_t &config );

    class RequestQueue : public ::ecpp::Containers::Queue<QueueAbleRequestBlock>, public ::sys::MutexLockable {};
    class RequestQueueItem;

  protected:
    ECPP_JOB_DECL(ProcessQueue);

    RequestQueue                      pending_requests_;
    i2c_port_t                        i2c_port_ { 0xff };

    void         Execute(RequestQueue::QueueItem &req);

    friend class TwiRequestBlock;
  };

  class TwiMaster::QueueAbleRequestBlock : public TwiRequestBlock
  {
  public:
    enum class State : uint8_t  {Idle, Queued, Pending, Success, Error};

    template<typename ...ARGS>
    void SetUp(ARGS ...args) { state_ = State::Idle; TwiRequestBlock::SetUp(args...); }

    bool IsActive()       const { return (state_ == State::Queued) || (state_ == State::Pending); }
    bool WasSuccessful()  const { return state_ == State::Success; }

  protected:
    Status  Execute(TwiMaster& master);
    Status  Execute(TwiMaster& master, uint_least16_t write_len, uint_least16_t read_len);

    State                       state_;
    friend class       TwiMaster;
  };

  class TwiMaster::RequestQueueItem : public TwiMaster::RequestQueue::QueueItem
  {
  public:
    void Submit(TwiMaster& drv, uint_fast16_t write_len, uint_fast16_t read_len);
  };

  template<typename Buffer, typename RequestBlock = TwiMaster::RequestQueueItem>
  class TwiBufferRequestBlock : public RequestBlock
  {
  public:
    void  SetUp(uint_fast8_t addr) { RequestBlock::SetUp(addr, &buffer_); }

    constexpr size_t GetBufferSize()           { return sizeof(buffer_); }
    constexpr ::ecpp::BinaryStruct GetStruct() { return {&buffer_, sizeof(Buffer)}; }
  protected:
    Buffer         buffer_;
  };
}

namespace signals
{
  void Complete(ecpp::Drivers::ESP32::TwiRequestBlock &req);
}

#endif