/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_DRIVERS_ESP32_SPI_HPP_
#define ECPP_DRIVERS_ESP32_SPI_HPP_

#include "driver/spi_master.h"
#include "Platform.hpp"

namespace ecpp::Drivers::ESP32
{
  class SpiDevice;
  class SpiMaster;

  class SpiRequestBlock
  {
  protected:
    enum class State : uint8_t     {Idle, Queued, Pending, Finished};

    spi_transaction_t transaction_ {};
    volatile State    state_       {State::Idle};

    virtual void PreTransfer() {};
    virtual void PostTransfer() {};
  public:
    constexpr SpiRequestBlock() {};

    void SetUp(uint8_t* dma_buffer) 
    { 
      transaction_.tx_buffer = dma_buffer;
      transaction_.rx_buffer = dma_buffer;
      state_ = State::Idle;
    }

    void Submit(SpiDevice& dev, uint_fast16_t send_len, uint_fast16_t recv_len);
    bool WaitFinished(SpiDevice& dev, ::sys::Tick::Delta ticks_to_wait);

    friend class SpiDevice;
  };

  class SpiDevice
  {
  public:
    void Initialize(SpiMaster& master, const spi_device_interface_config_t& config);

    static void PreTransferCB(spi_transaction_t *trans);
    static void PostTransferCB(spi_transaction_t *trans);

  protected:
    spi_device_handle_t handle_ { nullptr };

    friend class SpiRequestBlock;
  };

  class SpiMaster
  {
  public:
    constexpr SpiMaster(spi_host_device_t host_id) : host_id_(host_id) {}

    void Initialize(const spi_bus_config_t & bus_config);

  protected:
    const spi_host_device_t host_id_;

    friend class SpiDevice;
  };
}

namespace signals
{
  void TransferDone(::ecpp::Drivers::ESP32::SpiRequestBlock& srb);
}
#endif