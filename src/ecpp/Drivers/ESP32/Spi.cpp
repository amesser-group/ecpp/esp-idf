/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/Drivers/ESP32/Spi.hpp"
#include "ecpp/Container.hpp"

using namespace ecpp::Drivers::ESP32;


void
SpiRequestBlock::Submit(SpiDevice& dev, uint_fast16_t send_length, uint_fast16_t recv_length)
{
  transaction_.length   = send_length * 8;
  transaction_.rxlength = recv_length * 8;
  state_                = State::Queued;

  if(send_length == 0)
    transaction_.tx_buffer = nullptr;

  if(recv_length == 0)
    transaction_.rx_buffer = nullptr;

  spi_device_queue_trans(dev.handle_, &transaction_, portMAX_DELAY);
}


bool
SpiRequestBlock::WaitFinished(SpiDevice& dev, ::sys::Tick::Delta ticks_to_wait)
{
  const ::sys::Tick timeout(sys::GetTicks() + ticks_to_wait);

  while( (state_ == State::Queued) || 
         (state_ == State::Pending))
  {
    spi_transaction_t* trans;
    esp_err_t err;
    
    err = spi_device_get_trans_result(dev.handle_, &trans, 1 );
    
    if(err == ESP_OK)
    {
      container_of(SpiRequestBlock, transaction_, *trans).state_ = State::Finished;
    }
    else if (err == ESP_ERR_TIMEOUT)
    {
      if(timeout < sys::GetTicks())
        break;
    }
    else
    {
      break;
    }
  };
  
  return state_ == State::Finished;
}

void
SpiMaster::Initialize(const spi_bus_config_t & bus_config)
{
  spi_bus_initialize(host_id_, &bus_config, host_id_ - SPI2_HOST + 1);
}

void
SpiDevice::Initialize(SpiMaster& master, const spi_device_interface_config_t& config)
{
  spi_bus_add_device(master.host_id_, &config, &handle_);
}

void
SpiDevice::PreTransferCB(spi_transaction_t *trans)
{
  SpiRequestBlock& srb = container_of(SpiRequestBlock, transaction_, *trans);

  srb.state_ = srb.State::Pending;
  srb.PreTransfer();
}

void
SpiDevice::PostTransferCB(spi_transaction_t *trans)
{
  SpiRequestBlock& srb = container_of(SpiRequestBlock, transaction_, *trans);

  srb.PostTransfer();
  ::signals::TransferDone(srb);
}