/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPPSYS_LOG_HPP_
#define ECPPSYS_LOG_HPP_

#include <ecpp/Meta/Indices.hpp>
#include "esp_log.h"

namespace ecpp_sys
{
  template<size_t N>
  class Log;

  template<size_t N>
  constexpr Log<N> GetLogger(const char (&component)[N]);

  template<size_t N>
  class Log
  {
  public:
    template<typename ...Args>
    void Error(const char *fmt, Args && ...args) const
    {
      esp_log_write(ESP_LOG_ERROR, component_, fmt, args...);
    }

    template<typename ...Args>
    void Info(const char *fmt, Args && ...args) const
    {
      esp_log_write(ESP_LOG_INFO, component_, fmt, args...);
    }

  protected:
    template<ecpp::Meta::indices_type ...Is>
    constexpr Log(const char (&component)[N], ecpp::Meta::indices<Is...>) : component_{component[Is]...} {}

    constexpr Log(const char (&component)[N]) : Log(component, ecpp::Meta::build_indices<N>()) {}

    const char component_[N];

    friend Log GetLogger<N>(const char (&component)[N]);
  };

  template<size_t N>
  constexpr Log<N> GetLogger(const char (&component)[N])
  {
    return {component};
  }
}

#endif 